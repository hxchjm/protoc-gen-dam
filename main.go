package main

import (
	"flag"
	"fmt"
	"os"

	bmgen "gitee.com/hxchjm/protoc-gen-dam/generator"
	"gitee.com/hxchjm/protoc-gen-dam/pkg/gen"
	"gitee.com/hxchjm/protoc-gen-dam/pkg/generator"
)

// protoc --bm_out=. ./demo.proto
func main() {
	versionFlag := flag.Bool("version", false, "print version and exit")
	flag.Parse()
	if *versionFlag {
		fmt.Println(generator.Version)
		os.Exit(0)
	}

	g := bmgen.BmGenerator()
	gen.Main(g)
}
