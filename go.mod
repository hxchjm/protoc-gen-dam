module gitee.com/hxchjm/protoc-gen-dam

go 1.19

require (
	github.com/golang/protobuf v1.5.3
	github.com/pkg/errors v0.9.1
	github.com/siddontang/go v0.0.0-20180604090527-bdc77568d726
	google.golang.org/genproto/googleapis/api v0.0.0-20240125205218-1f4bbc51befe
)

require (
	google.golang.org/genproto v0.0.0-20240116215550-a9fa1716bcac // indirect
	google.golang.org/protobuf v1.32.0 // indirect
)
